from django.core.urlresolvers import reverse
from django.contrib import messages
from django.http import Http404, HttpResponseServerError
from django.template import loader, RequestContext
from django.conf import settings
from django.views.generic import TemplateView, FormView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token

from rawjam.core.utils.view_utils import MessageMixin

@requires_csrf_token
def server_error(request, template_name='500.html'):
	"""
	500 error handler.

	Templates: `500.html`
	Context: None
	"""
	t = loader.get_template(template_name) # You need to create a 500.html template.
	return HttpResponseServerError(t.render(RequestContext(request, {'request_path': request.path, 'STATIC_URL':settings.STATIC_URL, 'MEDIA_URL':settings.MEDIA_URL})))

"""
Base autocms views
"""
class Home(TemplateView):
	template_name = "autocms/home.html"

	def get_context_data(self, **kwargs):
		return {}
