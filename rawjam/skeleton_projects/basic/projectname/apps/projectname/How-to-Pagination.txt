HOW TO DO PAGINATION WITH AJAX CALL AND FILTERS:

Example based on pilot_v2 and mojo projects:

#####
VIEWS
#####

#### LANDING LIST VIEW ####
class ReportListView(ListView):
    model = Report
    template_name = 'report/reports.html'
    paginate_by = 1 # Number of items per page

    def get_queryset(self):
        qs = super(ReportListView, self).get_queryset()
        qs = qs.filter(visible=True)
        return qs

    def get_context_data(self, **kwargs):
        context = super(ReportListView, self).get_context_data(**kwargs)
        categories = ReportCategory.objects.filter()
        context['categories'] = categories
        return context


#### FILTER VIEW ####
class FilterReportsView(ListView):
    template_name = 'report/fragments/report-list.html'
    object_list = None
    model = Report
    paginate_by = 1 # Number of items per page

    def get_queryset(self):
        qs = super(FilterReportsView, self).get_queryset()

        category_slug = self.request.GET.get('category_slug', None)
        show_hidden = self.request.GET.get('show_hidden', False)
        show_hidden = True if show_hidden == 'true' else False

        qs = qs.filter(visible=not show_hidden)

        if category_slug:
            qs = qs.filter(category__slug=category_slug)
        return qs


#########
TEMPLATES
#########

##### reports.html #####
{% extends 'pilot_v2/ui/layouts/base.html' %}

{% block content %}
    <div class="row">
         <div class="col-xs-12 col-md-9 report-list">
             {% include 'report/fragments/report-list.html' %}
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="filter-block">
                <h4>Filter by Categories</h4>
                <select id="id-category-filter" name="category" placeholder="Filter by Categories">
                    <option></option>
                    {% for category in categories %}
                        <option value="{{ category.slug }}">{{ category.title }}</option>
                    {% endfor %}
                </select>
                <input type="checkbox" id="id-show-hidden"/><span> Show hidden reports</span>
            </div>
        </div>
    </div>
{% endblock %}

{% block extra_js %}
    <script>
        $(function() {
            var $select = $('#id-category-filter').selectize();
            var selectize = $select[0].selectize;

            #### AJAX CALL TO WHEN FILTERING BY CATEGORY ####
            selectize.on('change', function() {
                var categorySlug = selectize.getValue();
                var url = "{% url 'filter_reports' %}";
                var show_hidden = $('#id-show-hidden').prop('checked');
                $('#id-show-hidden').prop('checked', false);
                $.get(url, {
                    'show_hidden': show_hidden,
                    'category_slug': categorySlug,
                }, function(data) {
                    $('.report-list').html(data);
                    $('.toggle-report').bootstrapToggle('initialize');
                    $('#id-show-hidden').prop('checked', show_hidden);
                });
            });

            #### ANOTHER AJAX CALL TO FILTER ####
            $('#id-show-hidden').on('click', function() {
                var url = "{% url 'filter_reports' %}";
                var categorySlug = selectize.getValue();
                $.get(url, {
                    'show_hidden': $(this).prop('checked'),
                    'category_slug': categorySlug,
                }, function(data) {
                    $('.report-list').html(data);
                    $('.toggle-report').bootstrapToggle('initialize');
                    selectize.setValue(selectize.getValue());
                });
            });


            ##### IMPORTANT BLOCK #####
            $("body").on("click", ".pagination a", function(event) {
                event.preventDefault();
                var url = "URL_TO_YOUR_FILTER_VIEW";
                url = url + $(this).attr("href");
                $.get(url, {}, function(data){
                    $('.report-list').html(data);
                    $('.toggle-report').bootstrapToggle('initialize');
                }).fail(function(error){
                    console.log(error);
                });
            });
        })
    </script>
{% endblock %}


##### report-list.html #####
{% with reports=object_list %}
     {% for report in reports %}
         <div class="report-block"></div>
     {% endfor %}

    #### IMPORTANT BLOCK ####
    {% if is_paginated %}
        <div class="col-xs-12">
            {% with ajax=True %}

                #### THIS TEMPLATE IS INCLUDED IN RAWJAM SUBMODULE ####
                {% include "pagination/pagination.html" %}

            {% endwith %}
        </div>
    {% endif %}
{% endwith %}

{% block extra_js %}
    <script>
        $(function() {
            $('.toggle-report').change(function() {
                var $self = $(this);
                var reportId = $(this).data('report-id');
                var url = "/reports/ajax/report/" + reportId + "/update-visibility/";
                $self.closest('.report-block').addClass('hidden');

                $.post(url, {
                    'report_id': reportId,
                    'csrfmiddlewaretoken': csrftoken,
                    'visible': $(this).prop('checked')
                }).done(function(data) {
                });
            });
        });
    </script>
{% endblock %}