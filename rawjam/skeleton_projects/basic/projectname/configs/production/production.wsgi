import os, sys

# put the Django project on sys.path
sys.path.insert(0, '/home/ubuntu/sites/projectname/repository')
sys.path.insert(0, '/home/ubuntu/sites/projectname/repository/projectname')


os.environ["DJANGO_SETTINGS_MODULE"] = "projectname.configs.production.settings"

from django.core.handlers.wsgi import WSGIHandler
application = WSGIHandler()
