import os, django, urllib
from django.utils.translation import ugettext_lazy as _

#-------------------------------------------------------------------------------
#	BASE SETTINGS
#-------------------------------------------------------------------------------
DEBUG = True
TEMPLATE_DEBUG = DEBUG
INTERNAL_IPS = ('127.0.0.1',)
SECRET_KEY = '-x83)5-^cugn@*t6gh%76j@cb)zj)q7l_rm!%3=)@sw&v&d_ww'

DJANGO_ROOT = os.path.dirname(os.path.realpath(django.__file__))
SITE_ROOT = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
GIT_ROOT = os.path.join(SITE_ROOT, "../")

ADMIN_MEDIA_PREFIX = "/static/admin/"
STATIC_ROOT = os.path.join(SITE_ROOT, "static")
MEDIA_ROOT = os.path.join(SITE_ROOT, "media")
STATIC_URL = "/static/"
MEDIA_URL = "/media/"

ADMINS = (('Raw Jam Dev', 'dev@rawjam.co.uk'),)
MANAGERS = ADMINS

# Local time
TIME_ZONE = 'Europe/London'
LANGUAGE_CODE = 'en-gb'
SITE_ID = 1
USE_I18N = False
DATE_INPUT_FORMATS = ('%d-%m-%Y','%Y-%m-%d')

TEMPLATE_LOADERS = (
	'django.template.loaders.filesystem.Loader',
	'django.template.loaders.app_directories.Loader',
)

TEMPLATE_DIRS = (
	os.path.join(SITE_ROOT, 'templates'),
)

STATICFILES_FINDERS = (
	'django.contrib.staticfiles.finders.FileSystemFinder',
	'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

AUTHENTICATION_BACKENDS = (
	'django.contrib.auth.backends.ModelBackend',
	"allauth.account.auth_backends.AuthenticationBackend",
)

TEMPLATE_CONTEXT_PROCESSORS = (
	"django.contrib.auth.context_processors.auth",
	"django.core.context_processors.media",
	'django.core.context_processors.static',
	"django.core.context_processors.request",
	"django.contrib.messages.context_processors.messages",
	"rawjam.core.utils.context_processors.app_wide_vars",
	"allauth.account.context_processors.account",
	"allauth.socialaccount.context_processors.socialaccount",
)

MIDDLEWARE_CLASSES = [
	'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
]
TEMPLATE_DIRS = (
	os.path.join(SITE_ROOT, 'templates'),
)

INSTALLED_APPS = [
	# Base Django Apps
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'djangocms_admin_style',
	'admin_shortcuts',
	'django.contrib.admin',
	'django.contrib.sitemaps',
	'django.contrib.humanize',

	# Utilities & Helper Apps
	'rawjam.core',
	'filebrowser',
	'django_extensions',
	'endless_pagination',
	'djangular',

	# Registration, Signin and Account Management
	'allauth',
	'allauth.account',
	'allauth.socialaccount',
	'allauth.socialaccount.providers.twitter',
	'allauth.socialaccount.providers.facebook',
	'allauth.socialaccount.providers.google',

	# Local Project Apps
	'apps.projectname_profile',
	'apps.autocms',
]

if DEBUG:
	TEMPLATE_CONTEXT_PROCESSORS += ('django.core.context_processors.debug',)
	#MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',) # Currently not working with Django 1.7 so commented out for now
	#INSTALLED_APPS += ('debug_toolbar',)

if USE_I18N:
	TEMPLATE_CONTEXT_PROCESSORS += ('django.core.context_processors.i18n',)

SERIALIZATION_MODULES = {
	'json': 'rawjam.contrib.wadofstuff.django.serializers.custom_json'
}


#-------------------------------------------------------------------------------
#	APP SETTINGS
#-------------------------------------------------------------------------------
RAWJAM_WEBSITE = 'http://rawjam.co.uk/'
RAWJAM_TITLE = 'Creative ideas, brought to life'
TEST_RUNNER = 'django.test.runner.DiscoverRunner'
DEFAULT_PROFILE_IMAGE = os.path.join(MEDIA_ROOT, 'uploads/projectname_defaults/profile_portrait_default.png')
GOOGLE_ANALYTICS_CODE = None
LANGUAGES = (
    ('en', _('English')),
)

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': 'autocms',
		'USER': 'benjamindell',
		'PASSWORD': 'edenhouse',
		'HOST': 'localhost',
	}
}
ROOT_URLCONF = 'autocms.configs.common.urls'
AUTH_PROFILE_MODULE = 'projectname_profile.UserProfile'

DEFAULT_FROM_EMAIL = 'noreply@rawjam.co.uk'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = 'noreply@rawjam.co.uk'
EMAIL_HOST_PASSWORD = '2M5394'


#-------------------------------------------------------------------------------
#	CACHE SETTINGS
#-------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
		'KEY_PREFIX': 'autocms:'
    }
}
CACHE_MIDDLEWARE_ALIAS = 'default'
CACHE_MIDDLEWARE_SECONDS = 60
CACHE_MIDDLEWARE_KEY_PREFIX = 'autocms:'

#-------------------------------------------------------------------------------
#	ALLAUTH SETTINGS
#-------------------------------------------------------------------------------
LOGIN_URL = '/accounts/login/'
LOGIN_REDIRECT_URL = "/"
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = False
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_LOGOUT_ON_GET = True
SOCIALACCOUNT_QUERY_EMAIL = True
SOCIALACCOUNT_AUTO_SIGNUP = True
SOCIALACCOUNT_AVATAR_SUPPORT = False
EMAIL_CONFIRMATION_DAYS = 99
FACEBOOK_ENABLED = True
TWITTER_ENABLED = True
OPENID_ENABLED = False
SOCIALACCOUNT_ENABLED = True
SOCIALACCOUNT_PROVIDERS = {}


#-------------------------------------------------------------------------------
#	FILEBROWSER SETTINGS
#-------------------------------------------------------------------------------
FILEBROWSER_URL_FILEBROWSER_MEDIA = STATIC_URL + "filebrowser/"
FILEBROWSER_PATH_FILEBROWSER_MEDIA = os.path.join(STATIC_URL, 'filebrowser/')
FILEBROWSER_EXTENSIONS = {
	'Folder': [''],
	'Image': ['.jpg','.jpeg','.gif','.png','.tif','.tiff'],
	'Video': ['.mov','.wmv','.mpeg','.mpg','.avi','.rm','.swf'],
	'Document': ['.pdf','.doc','.rtf','.txt','.xls','.csv'],
	'Sound': ['.mp3','.mp4','.wav','.aiff','.midi','.m4p'],
	'Code': ['.html','.py','.js','.css'],
}
FILEBROWSER_ADMIN_THUMBNAIL = 'fb_thumb'
FILEBROWSER_IMAGE_MAXBLOCK = 1024*1024
FILEBROWSER_MAX_UPLOAD_SIZE = 10485760 # 10485760 bytes = about 10megs
FILEBROWSER_VERSIONS = {
	'fb_thumb': {'verbose_name': 'Admin Thumbnail', 'width': 60, 'height': 60, 'opts': 'crop upscale'},
	'thumb': {'verbose_name': 'Grid Thumb', 'width': 150, 'height': 150, 'opts': 'crop upscale'},
	'small': {'verbose_name': 'Small (210px)', 'width': 210, 'height': '', 'opts': ''},
	'medium': {'verbose_name': 'Medium (370px)', 'width': 370, 'height': '', 'opts': ''},
	'large': {'verbose_name': 'Large (530px)', 'width': 530, 'height': '', 'opts': ''},
}
FILEBROWSER_ADMIN_VERSIONS = [
	'thumb', 'small','medium','large',
]


#-------------------------------------------------------------------------------
#	ADMIN SHORTCUT SETTINGS
#-------------------------------------------------------------------------------
ADMIN_SHORTCUTS = [
	{
		'shortcuts': [
			{
				'url': '/',
				'open_new_window': True,
			},
			{
				'url': '/admin/filebrowser/browse/',
				'title': 'Files',
				'class': 'folder'
			},
			{
				'url_name': 'admin:auth_user_changelist',
				'title': 'Users',
			},
		]
	},
]
ADMIN_SHORTCUTS_SETTINGS = {
	'hide_app_list': False,
	'open_new_window': False,
}

try:
	from local_settings import *
except ImportError:
	pass
