module.exports = function(grunt) {
    var appConfig = grunt.file.readJSON('package.json');

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    var pathsConfig = function (appName) {
        this.app = appName || appConfig.name;
        this.localStaticRoot = this.app + "/apps/" + this.app + "/static";
        this.buildRoot = this.app + "/build";
        this.staticRoot = this.buildRoot + "/static";

        return {
            appRoot: this.app,
            localStaticRoot: this.localStaticRoot,
            staticRoot: this.staticRoot,
            buildRoot: this.buildRoot,
            angularTemplates: this.localStaticRoot + '/client/views',
            css: this.localStaticRoot + '/css',
            less: this.localStaticRoot + '/less',
            fonts: this.localStaticRoot + '/fonts',
            images: this.localStaticRoot + '/img',
            js: this.localStaticRoot + '/js',
            jsClient: this.localStaticRoot + '/client'
        };
    };

    var path = require('path');
    var createDjangoStaticConcatConfig = function(context, block) {
        var cfg = {files: []};
        var staticPattern = /\{\{\s*STATIC_URL\s*\}\}/;

        block.dest = block.dest.replace(staticPattern, 'static/');
        var outfile = path.join(context.outDir, block.dest);

        // Depending whether or not we're the last of the step we're not going to output the same thing
        var files = {
            dest: outfile,
            src: []
        };
        context.inFiles.forEach(function(f) {
            files.src.push(path.join(context.inDir, f.replace(staticPattern, '')));
        });
        cfg.files.push(files);
        context.outFiles = [block.dest];

        return cfg;
    };

    // Project configuration.
    grunt.initConfig({
        paths: pathsConfig(),
        pkg: appConfig,

        ngAnnotate: {
            options: {
                singleQuotes: true,
            },
            files: {
                '<%= paths.buildRoot %>/static/dist/js/client.min.js': ['<%= paths.buildRoot %>/static/dist/js/client.min.js']
            },
        },

        concat: {
            options: {
                separator: ';',
                // // Replace all 'use strict' statements in the code with a single one at the top
                // banner: "'use strict';\n",
                // process: function(src, filepath) {
                //    return '// Source: ' + filepath + '\n' +
                //    src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
                // },
            },
            dist: {}
        },

        uglify: {
            options: {
                'sourceMap': true
            },
            dist: {}
        },

        less: {
            dev: {
                options: {
                    paths: ['<%= paths.less %>'],
                    compress: false,
                    yuicompress: false
                },
                files: {
                    '<%= paths.localStaticRoot %>/dist/css/theme.css': '<%= paths.less %>/theme.less'
                }
            },
            dist: {
                options: {
                    paths: ['<%= paths.less %>'],
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    '<%= paths.buildRoot %>/static/dist/css/theme.css': '<%= paths.less %>/theme.less'
                }
            },
        },

        html2js: {
            options: {
                base: '<%= paths.appRoot %>/apps/<%= paths.appRoot %>',
            },
            main: {
                src: ['<%= paths.angularTemplates %>/**/*.html'],
                dest: '<%= paths.jsClient %>/templates.js'
            },
        },

        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= paths.buildRoot %>/img',
                    src: '{,*/}*.{png,jpg,jpeg}',
                    dest: '<%= paths.buildRoot %>/img'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= paths.buildRoot %>/img',
                    src: '{,*/}*.svg',
                    dest: '<%= paths.buildRoot %>/img'
                }]
            }
        },

        clean: [
            '<%= paths.buildRoot %>/static/dist/*',
        ],

        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 8
            },
            css: {
                src: [
                    '<%= paths.buildRoot %>/static/dist/css/{,*/}*.css',
                    //'<%= paths.buildRoot %>/static/img/{,*/}*.{png,jpg,jpeg,gif,webp}'
                ],
                dest: '<%= paths.buildRoot %>/static/dist/css'
            },
            js: {
                src: [
                    '<%= paths.buildRoot %>/static/dist/js/{,*/}*.js',
                    //'<%= paths.buildRoot %>/static/img/{,*/}*.{png,jpg,jpeg,gif,webp}'
                ],
                dest: '<%= paths.buildRoot %>/static/dist/js'
            }
        },

        cssmin: {
            options: {
                report: 'gzip'
            },
            dist: {}
        },

        useminPrepare: {
            html: [
                '<%= paths.buildRoot %>/templates/<%= paths.appRoot %>/ui/layouts/base.html',
            ],
            css: ['<%= paths.buildRoot %>/static/dist/css/theme.css'],
            options: {
                dest: '<%= paths.buildRoot %>/static',
                root: '<%= paths.buildRoot %>/static',
                flow: {
                    assetsDirs: ['<%= paths.buildRoot %>/static/img'],
                    steps: {
                        js: [
                            {
                                name: 'concat',
                                createConfig: createDjangoStaticConcatConfig
                            },
                            'uglifyjs'
                        ],
                        css: [
                            {
                                name: 'cssmin',
                                createConfig: createDjangoStaticConcatConfig
                            }
                        ]
                    },
                    post: {}
                }
            }
        },

        usemin: {
            html: [
                '<%= paths.buildRoot %>/templates/<%= paths.appRoot %>/ui/layouts/base.html',
            ],
            options: {
                assetsDirs: [
                    '<%= paths.buildRoot %>/static',
                ],
                dest: '<%= paths.buildRoot %>',
                root: '<%= paths.buildRoot %>/static',
                patterns: {
                    html: [
                        [
                            /<script.+src=['"dist\/]([^"']+)["']/gm,
                            "Add Django's {{ STATIC_URL }} to the script tag",
                            null,
                            function(match) {
                                return match.replace('dist/', '{{ STATIC_URL }}dist/');
                            }
                        ],
                        [
                            /<link[^\>]+href=['"dist\/]([^"']+)["']/gm,
                            "Add Django's {{ STATIC_URL }} to the style link tag",
                            null,
                            function(match) {
                                return match.replace('dist/', '{{ STATIC_URL }}dist/');
                            }
                        ],
                    ]
                }
            }
        },

        copy:{
            initial: {
                files: [
                    {src: '<%= paths.appRoot %>/templates/<%= paths.appRoot %>/ui/layouts/base.html', dest:'<%= paths.buildRoot %>/templates/<%= paths.appRoot %>/ui/layouts/base.html'},
                    {expand: true, cwd:'<%= paths.staticRoot %>', src: ['**'], dest:'<%= paths.buildRoot %>/static/'}
                ]
            }
        },

        watch: {
            /*options: {
                livereload: 12345,
                spawn: false,
            },*/
            less: {
                files: ['<%= paths.less %>/**/*.less'],
                tasks: ['less:dev']
            },
            angularTemplates: {
                files: ['<%= paths.angularTemplates %>/**/*.html'],
                tasks: [
                    'html2js'
                ]
            },
            // livereload: {
            //     files: [
            //         '<%= paths.js %>/**/*.js',
            //         '<%= paths.less %>/**/*.less'
            //     ],
            //     options: {
            //         spawn: false,
            //         livereload: true,
            //     },
            // },
        },
    });

    grunt.registerTask('dist', [
        'clean',
        'copy:initial',
        'less:dist',
        'useminPrepare',
        'concat',
        'ngAnnotate',
        'uglify',
        'cssmin',
        'filerev',
        'usemin',
        'imagemin',
        'svgmin'
    ]);

    grunt.registerTask('dev', [
        'less:dev',
        'html2js',
        'watch'
    ]);

    grunt.registerTask('default', [
        'dev'
    ]);
};
