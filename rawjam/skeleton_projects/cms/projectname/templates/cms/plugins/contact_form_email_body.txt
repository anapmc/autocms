<p>A contact form has been submitted with the following details:</p>
<br/>

<b>Name</b>: {{ name }}<br/>
<b>Email</b>: {{ email }}<br/>
<b>Message</b>: {{ message }}<br/>

<br/>
Thank you.
