from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse

from filebrowser.fields import FileBrowseField
from cms.models.pluginmodel import CMSPlugin
from cms.models.fields import PlaceholderField
from djangocms_text_ckeditor.fields import HTMLField

from rawjam.core.utils.abstract_models import BaseModel, TitleAndSlugModel

"""
Example Models for a Django CMS project with a news app

class NewsItem(TitleAndSlugModel):
	body = models.TextField(null=True, blank=True)

	def get_absolute_url(self):
		return reverse('news_detail', args=[self.slug])

class TestimonialItem(BaseModel):
	name = models.CharField(max_length=150)
	position = models.CharField(max_length=150)
	location = models.CharField(max_length=150)
	body = models.TextField(null=True, blank=True)


# CMS PLUGIN MODELS

class TitleImageAndTextPluginModel(CMSPlugin):

	BACKGROUND_CHOICES = (
		('standard', 'Standard'),
		('light-grey', 'Light grey')
	)

	OPTIONS_CHOICES = (
		('standard', 'Standard'),
		('no-bottom-padding', 'No bottom padding')
	)

	IMAGE_POSITION_CHOICES = (
		(0, 'Left'),
		(1, 'Right')
	)

	title = models.CharField(max_length=150, null=True, blank=True)
	image = FileBrowseField(directory="page_media", max_length=255, null=True, blank=True)
	image_position = models.PositiveIntegerField(choices=IMAGE_POSITION_CHOICES, default=0)
	body = HTMLField(null=True, blank=True)
	background = models.CharField(max_length=25, choices=BACKGROUND_CHOICES, default='standard')
	options = models.CharField(max_length=25, choices=OPTIONS_CHOICES, default='standard')
	use_title_from_page = models.BooleanField(default=False)

class TitleAndTextPluginModel(CMSPlugin):

	BACKGROUND_CHOICES = (
		('standard', 'Standard'),
		('light-grey', 'Light grey')
	)

	OPTIONS_CHOICES = (
		('standard', 'Standard'),
		('no-bottom-padding', 'No bottom padding')
	)

	CONTENT_LAYOUT_CHOICES = (
		('standard', 'Standard'),
		('centered', 'Centered')
	)

	title = models.CharField(max_length=150)
	body = HTMLField(null=True, blank=True)
	background = models.CharField(max_length=25, choices=BACKGROUND_CHOICES, default='standard')
	options = models.CharField(max_length=25, choices=OPTIONS_CHOICES, default='standard')
	content_layout = models.CharField(max_length=25, choices=CONTENT_LAYOUT_CHOICES, default='standard')

class ColumnsPluginModel(CMSPlugin):
	column_1 = PlaceholderField('column_1', related_name="column_1")
	column_2 = PlaceholderField('column_2', related_name="column_2")
	column_3 = PlaceholderField('column_3', related_name="column_3")
	column_4 = PlaceholderField('column_4', related_name="column_4")
	qty_columns = models.IntegerField(default=4)
	extra_column_class = models.CharField(max_length=150, null=True, blank=True)

	@property
	def bootstrap_col_class(self):
		if self.qty_columns == 1: return 'col-xs-12'
		elif self.qty_columns == 2: return 'col-xs-6'
		elif self.qty_columns == 3: return 'col-xs-4'
		elif self.qty_columns == 4: return 'col-xs-3'

	def save(self, *args, **kwargs):
		from cms.api import add_plugin

		if not self.pk:
			super(ColumnsPluginModel, self).save(*args, **kwargs)
			add_plugin(self.column_1, 'TextPlugin', settings.LANGUAGE_CODE, body="<p>Add content here</p>")
			add_plugin(self.column_2, 'TextPlugin', settings.LANGUAGE_CODE, body="<p>Add content here</p>")
			add_plugin(self.column_3, 'TextPlugin', settings.LANGUAGE_CODE, body="<p>Add content here</p>")
			add_plugin(self.column_4, 'TextPlugin', settings.LANGUAGE_CODE, body="<p>Add content here</p>")
		else:
			super(ColumnsPluginModel, self).save(*args, **kwargs)

class ContactPluginModel(CMSPlugin):
	recipient = models.EmailField(max_length=150, help_text="Who should receive notifications of contact submissions?")

class NewsItemPluginModel(CMSPlugin):
	pass

class TestimonialSlideshowPluginModel(CMSPlugin):
	pass
"""