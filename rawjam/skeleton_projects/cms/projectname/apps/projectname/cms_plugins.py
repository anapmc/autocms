from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse
from django.utils import simplejson

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin

from rawjam.core.utils.comms import create_threaded_email

from models import *

"""
Example Django CMS plugins

class TitleImageAndTextPlugin(CMSPluginBase):
	model = TitleImageAndTextPluginModel
	name = _("Title, image and text plugin")
	render_template = "cms/plugins/title_image_text.html"

	def render(self, context, instance, placeholder):
		context['instance'] = instance
		return context
plugin_pool.register_plugin(TitleImageAndTextPlugin)


class TitleAndTextPlugin(CMSPluginBase):
	model = TitleAndTextPluginModel
	name = _("Title and text plugin")
	render_template = "cms/plugins/title_text.html"
	allow_children = True

	def render(self, context, instance, placeholder):
		context['instance'] = instance
		return context
plugin_pool.register_plugin(TitleAndTextPlugin)


class ColumnsPlugin(CMSPluginBase):
	model = ColumnsPluginModel
	name = _("Columns")
	render_template = "cms/plugins/columns.html"

	def render(self, context, instance, placeholder):
		context['instance'] = instance
		return context
plugin_pool.register_plugin(ColumnsPlugin)


class ContactPlugin(CMSPluginBase):
	model = ContactPluginModel
	name = _("Contact form")
	render_template = "cms/plugins/contact_form.html"

	def render(self, context, instance, placeholder):
		context['instance'] = instance
		request = context['request']

		if request.method == "POST":
			name = request.POST.get('name', None)
			email = request.POST.get('email', None)
			message = request.POST.get('message', None)

			files = []
			if request.FILES:
				files.append({
					'name': request.FILES['attachment'].name,
					'content': request.FILES['attachment'].read(),
					'type': request.FILES['attachment'].content_type
				})

			if (name and email and message):
				template_context = {'name': name, 'email': email, 'message': message}
				create_threaded_email(request, "Contact form submission", None,
					[instance.recipient,], template="cms/plugins/contact_form_email_body.txt",
					template_context=template_context, attachments=files, content_subtype='html')

			return context
		else:
			return context
plugin_pool.register_plugin(ContactPlugin)


class NewsItemPlugin(CMSPluginBase):
	model = NewsItemPluginModel
	name = _("News list")
	render_template = "cms/plugins/news_list.html"

	def render(self, context, instance, placeholder):
		context['instance'] = instance

		news = NewsItem.objects.all()
		context['news'] = news

		return context
plugin_pool.register_plugin(NewsItemPlugin)


class TestimonialSlideshowPlugin(CMSPluginBase):
	model = TestimonialSlideshowPluginModel
	name = _("Testimonial slideshow")
	render_template = "cms/plugins/testimonial_slideshow.html"

	def render(self, context, instance, placeholder):
		context['instance'] = instance

		testimonials = TestimonialItem.objects.all()
		context['testimonials'] = testimonials

		return context
plugin_pool.register_plugin(TestimonialSlideshowPlugin)
"""