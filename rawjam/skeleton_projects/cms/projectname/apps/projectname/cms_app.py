from django.utils.translation import ugettext_lazy as _

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

"""
Example Apphook. Read here (http://django-cms.readthedocs.org/en/latest/how_to/apphooks.html) for more information.


class NewsApphook(CMSApp):
	name = _("News Apphook")
	urls = ["apps.autocms.urls.news"]

apphook_pool.register(NewsApphook)
"""