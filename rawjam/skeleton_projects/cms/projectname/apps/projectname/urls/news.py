from django.conf.urls import *
from apps.projectname.views import *

urlpatterns = patterns('',
	url(r'^(?P<slug>[\w-]+)/$', NewsDetail.as_view(), name='news_detail'),
)
