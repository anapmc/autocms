#!/bin/bash
set -e
LOGFILE=/home/ubuntu/logs/projectname_gunicorn.access.log
ERRORFILE=/home/ubuntu/logs/projectname_gunicorn.error.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=3  #recommended formula here is 1 + 2 * NUM_CORES

#we don't want to run this as root..
USER=www-data
GROUP=www-data

ADDRESS=unix:/var/run/projectname.sock

cd /home/ubuntu/sites/projectname/repository
source ../env/bin/activate
export PYTHONPATH=$PYTHONPATH:/home/ubuntu/sites/projectname/repository/rawjam
export PYTHONPATH=$PYTHONPATH:/home/ubuntu/sites/projectname/repository/projectname
test -d $LOGDIR || mkdir -p $LOGDIR
exec gunicorn_django -w $NUM_WORKERS --bind=$ADDRESS \
	--log-level=debug \
	--log-file=$LOGFILE 2>>$LOGFILE  1>>$ERRORFILE \
	--settings=projectname.configs.production.settings \
	--user=$USER --group=$GROUP
