from projectname.configs.common.settings import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG
PROJECT_DOMAIN = "http://autocms.com"

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'autocms',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
    }
}

ADMINS = (('Raw Jam Dev', 'dev@rawjam.co.uk')),
MANAGERS = ('Raw Jam Dev', 'dev@rawjam.co.uk'),

ALLOWED_HOSTS = [
    '.autocms.com',
]

PIPELINE_ENABLED = not DEBUG
PIPELINE_CSS = {
	'theme': {
		'source_filenames': (
			'css/chosen.css',
			'less/theme.less' if not DEBUG else ''
		),
		'output_filename': 'css/theme.min.css',
		'variant': 'datauri'
	},
}
