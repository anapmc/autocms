'use strict';

// Setup the app
var AngularApp = angular.module('AngularApp', [
	'ngCookies', 'ngRoute', 'ngAnimate', 'restangular',
	'notifications', 'filters', 'AuthService',
	'UserModel', 'UserService'
	])
	.run(function($http, $cookies) {
		$http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
		$http.defaults.xsrfCookieName = 'csrftoken';
		$http.defaults.xsrfHeaderName = 'X-CSRFToken';
	})
	.run(function ($rootScope, AuthService, UserModel) {
		// Get the logged in user
		AuthService.getLoggedInUser().then(function(response) {
			if (response.pk > 0) {
				var obj = {};
				$.extend(obj, UserModel);
				obj.init(response);
				$rootScope.loggedInUser = obj;
			}
			else {
				$rootScope.loggedInUser = null;
			}
		});
	})
	.config(function($interpolateProvider) {
		$interpolateProvider.startSymbol('{$');
		$interpolateProvider.endSymbol('$}');
	})
	.config(function(RestangularProvider) {
		RestangularProvider.setBaseUrl('/crud');
		RestangularProvider.setRequestSuffix('\/');
		RestangularProvider.setRestangularFields({id: "pk"});
	})
	.constant('msdElasticConfig', {
		append: ''
	})
	.config(['$routeProvider',
		function($routeProvider) {
			$routeProvider.
			when('/page-one', {
				templateUrl: '/static/client/views/page_one.html',
				controller: 'PageOneController'
			}).
			when('/page-two', {
				templateUrl: '/static/client/views/page_two.html'
			}).
			otherwise({
				redirectTo: '/page-one'
			});
		}
	]);

/**
* Simple namespace util to extand Class.js functionality
* and wrap classes in namespace.
* @author tommy.rochette[followed by the usual sign]universalmind.com
* @type {*}
* @return Object
*/
window.namespace = function(namespaces){
	'use strict';
	var names = namespaces.split('.');
	var last  = window;
	var name  = null;
	var i     = null;

	for(i in names){
		name = names[i];

		if(last[name]===undefined){
			last[name] = {};
		}

		last = last[name];
	}
	return last;
};

String.prototype.toCamel = function(){
	return this.replace(/(\-[a-z])/g, function($1){return $1.toUpperCase().replace('-','');});
};

moment.lang('en', {
	'calendar' : {
        sameDay : '[Today]',
        nextDay : '[Tomorrow]',
        nextWeek : 'dddd',
        lastDay : '[Yesterday]',
        lastWeek : '[Last] dddd',
        sameElse : 'L'
    }
});
