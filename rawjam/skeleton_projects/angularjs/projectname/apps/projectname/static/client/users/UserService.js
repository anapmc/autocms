var UserService = BaseService.extend({
	_restangularModelName: 'users',
});
UserService.$inject = ['Restangular'];

(function (){

	var UserServiceProvider = Class.extend({

		instance: new UserService(),

		$get:['Restangular', function(Restangular){
			this.instance.restangular = Restangular.withConfig(function(Configurer) {
				//Configurer.setBaseUrl('/crud');
			});
			return this.instance;
		}],
	})

	angular.module('UserService', [])
		.provider('UserService', UserServiceProvider);
}());
