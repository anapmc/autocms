var BaseService = EventDispatcher.extend({
	_restangularModelName: null,
	_relations: {},

	updateField:function(id, field, value){
		var qs = {'relations':this._relations};
		qs[field] = value;
		return this.restangular.all(this._restangularModelName).customOperation('patch', id, qs).then(function(response) {
			return response;
		});
	},
	delete:function(model){
		var modelId = model.id;
		return this.restangular.all(this._restangularModelName).customOperation('remove', modelId).then(function(response) {
			return modelId;
		});
	},
	get:function(id){
		return this.restangular.all(this._restangularModelName).customGET(id, {'relations':this._relations});
	},
	getAll:function(){
		return this.restangular.all(this._restangularModelName).getList({'relations':this._relations});
	},
	add:function(data){
		return this.restangular.all('').customPOST(data, this._restangularModelName);
	}
});

BaseService.$inject = ['Restangular'];
