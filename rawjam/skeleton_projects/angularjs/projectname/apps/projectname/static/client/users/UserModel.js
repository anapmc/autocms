/**
 * User events
 */
namespace('users.events').USER_LOADED = "UserModel.USER_LOADED";

/**
 * User model
 */
var UserModel = EventDispatcher.extend({
	// Injected by the provider
	_routes:null,
	_location:null,
	_userService:null,

	/*
	* Model properties
	*/
	id: null,
	firstName: null,
	lastName: null,
	username: null,
	email: null,
    profileImage: null,

    safeFirstName: function() {
        if (this.firstName) return this.firstName;
        return this.username;
    },
	fullName: function(){
		if (this.firstName && this.lastName)
			return this.firstName + ' ' + this.lastName;
		else if (this.firstName)
			return this.firstName;
		else
			return this.username;
	},
	title: function(){
		return this.fullName();
	},
    image: function(){
        return this.profileImage;
    },

	/*
	* CRUD functions
	*/
	_getUser:function(id){
		this._usersService.get(id)
			.then(this._handleGetUserSuccess.bind(this),this._handleGetUserError.bind(this));
	},
	_handleGetUserSuccess:function(result){
		this.dispatchEvent(users.events.USER_LOADED);
	},
	_handleGetUserError:function(error){
		console.error('UserModel : _getUser error');
	},

	/*
	* Init
	*/
	initProfile: function(initData){
		if (initData) {
            this.initUser(initData.user);
            this.profileImage = initData.profile_image;
		}
	},
    init: function(initData){
        if (initData) {
            this.id = initData.pk;
            this.firstName = initData.first_name;
            this.lastName = initData.last_name;
            this.username = initData.username;
            this.email = initData.email;
            if (initData.profile) {
                this.profileImage = initData.profile.profile_image;
            }
        }
    },
});


/**
 * User provider
 */
(function (){

	var UserModelProvider = Class.extend({
		instance: new UserModel(),

		/**
        * Initialize and configure UserModel
        * @return UserModel
        */
		$get:['$location', '$route', 'UserService',function($location, $route, UserService){
			this.instance._routes = $route;
			this.instance._location = $location;
			this.instance._userService = UserService;
			return this.instance;
		}]
	});

	angular.module('UserModel', [])
		.provider('UserModel', UserModelProvider);
}());
