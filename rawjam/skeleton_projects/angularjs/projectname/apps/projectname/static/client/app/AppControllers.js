var AppController = BaseController.extend({
    $upload: null,
    _notifications:null,

    init:function($scope, $timeout, Notifications, $route){
        this.$timeout = $timeout;
        this._notifications = Notifications;
        this._super($scope);
    },

    /*
    * Listeners
    */
    defineListeners:function(){
        this._super();
    },

    /*
    * Scope properties and methods
    */
    defineScope:function(){
        var self = this;
        this.$scope.now = moment();
    },

    /*
    * Clean up
    */
    destroy:function(){

    }
})
AppController.$inject = ['$scope', '$timeout', 'Notifications', '$route'];

var PageOneController = BaseController.extend({
    $upload: null,
    _notifications:null,

    init:function($scope, $timeout, Notifications, $route){
        this.$timeout = $timeout;
        this._notifications = Notifications;
        this._super($scope);
    },

    /*
    * Listeners
    */
    defineListeners:function(){
        this._super();
    },

    /*
    * Scope properties and methods
    */
    defineScope:function(){
        var self = this;
        this.$scope.now = moment();
    },

    /*
    * Clean up
    */
    destroy:function(){

    }
})
PageOneController.$inject = ['$scope', '$timeout', 'Notifications', '$route'];
