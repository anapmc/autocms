AngularApp.directive('onEnter', function() {
    return {
        scope: {
          onEnter: '&onEnter'
        },
        link: function(scope, element) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.onEnter({event:event});
                    scope.$apply();
                }
            });
        }
    }
});

// Focus toggles
AngularApp.directive('focusToggle', function($timeout) {
    return {
        restrict: 'E',
        require: '?ngModel',
        scope: {
            ngModel: '=',
            extraCss: '=',
            handleSave: '&onSave'
        },
        link: function(scope, elm, attr) {

            elm.find('.input.toggle').toggleInput({
                on: function(element){
                    scope.ngModel = true;
                    scope.$apply();
                    if (scope.handleSave)
                      scope.save();
                },
                off: function(element){
                    scope.ngModel = false;
                    scope.$apply();
                    if (scope.handleSave)
                      scope.save();
                }
            });

            scope.save = function() {
              scope.handleSave({value: scope.ngModel});
            };
        },
        templateUrl: '/static/client/base/views/fragments/focus_toggle.html'
    };
});

// Equal height columns
AngularApp.directive('equalHeight', function($timeout) {
    return {
		restrict: 'A',
		link: function(scope, elem, attrs) {
			var equalizeColumns = function() {
				if ($(window).width() > 768) {
					var maxHeight = 0;
					elem.find('.column-same-size').css('height', 'auto');
					elem.find('.column-same-size').each(function(index, element){
						var height = $(this).outerHeight();
						if(height > maxHeight)
							maxHeight = height;
					});
					elem.find('.column-same-size').css('height', maxHeight+'px');
				}
			}

			$timeout(function() {
				equalizeColumns();
			}, 1500);
		}
    };
});

// Inline edit directive
AngularApp.directive('inlineEdit', function($timeout, $sce, stripTagsFilter, lineBreaksBrFilter, brToNewLineFilter) {
	return {
		scope: {
			model: '=inlineEdit',
			ifNoValue: '=ifNoValue',
			maxLength: '=maxLength',
			disableEnterKeySubmit: '=',
			handleSave: '&onSave',
			handleCancel: '&onCancel'
		},
		link: function(scope, elm, attr) {
			var previousValue;

			scope.ifNoValueTrustedHtml = $sce.trustAsHtml(scope.ifNoValue);
			scope.modelTrustedHtml = $sce.trustAsHtml(lineBreaksBrFilter(stripTagsFilter(scope.model)));

			scope.edit = function() {
				scope.model = brToNewLineFilter(stripTagsFilter(scope.model));

				scope.editMode = true;
				previousValue = scope.model;

				$timeout(function() {
					elm.find('textarea')[0].focus();
					elm.find('textarea')
					    .on('keyup', function(e){
					        if (e.which == 27) { // Escape
								this.blur();
					        	scope.cancel();
								scope.$apply();
					        }
							else if (e.which == 13 && !scope.disableEnterKeySubmit) { // Enter
								this.blur();
								scope.save();
								scope.$apply();
							}
					    });
				}, 0, false);
			};
			scope.save = function() {
				scope.modelTrustedHtml = $sce.trustAsHtml(lineBreaksBrFilter(stripTagsFilter(scope.model)));
				scope.editMode = false;
				scope.handleSave({value: scope.model});
			};
			scope.cancel = function() {
				scope.modelTrustedHtml = $sce.trustAsHtml(lineBreaksBrFilter(stripTagsFilter(scope.model)));
				scope.editMode = false;
				scope.model = previousValue;
				scope.handleCancel({value: scope.model});
			};
		},
		templateUrl: '/static/client/base/views/fragments/inline_edit.html'
	};
});

// Action data dropdown
AngularApp.directive('actionDataDropdown', function($timeout) {
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			html: '=actionDataDropdown',
			data: '=',
			model: '=',
			forcePosition: '=',
			selectedIds: '=',
			staticUrl: '=',
			handleClick: '&onClick'
		},
		requires: 'itemIsSelected',
		link: function(scope, elm, attr, controller) {

			$timeout(function(){
				scope.position = scope.forcePosition;
				if (!scope.position)
					scope.position = 'left';
			});

			scope.itemClicked = function(e, itemID) {
				var elem = angular.element(e.srcElement);
				var elemTitle = elem.parent().find('.item-title').html();
				var selectType = 1;
				if (scope.itemIsSelected(itemID))
					selectType = 0;
				scope.handleClick({'parentID':scope.model.id, 'itemID':itemID, 'selectType':selectType, 'title':elemTitle});
			};
			scope.selectedCssClass = function(itemID) {
				if (scope.itemIsSelected(itemID))
					return 'selected';
				else
					return 'unselected';
			};
			scope.itemIsSelected = function(itemID) {
				if (scope.selectedIds) {
					if( typeof scope.selectedIds === 'object' ) {
						return scope.selectedIds.indexOf(itemID) != -1;
					}
					else {
						if (scope.selectedIds == itemID)
							return true;
						else
							return false;
					}
				}
				else
					return false;
			};
		},
		templateUrl: '/static/client/base/views/fragments/action_data_dropdown.html'
	};
});

// Field update popover
AngularApp.directive('fieldUpdatePopover', function($timeout) {
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			html: '=fieldUpdatePopover',
			field: '=',
			staticUrl: '=',
			handleClick: '&onClick'
		},
		requires: 'field',
		link: function(scope, elm, attr, controller) {

		},
		templateUrl: '/static/client/base/views/fragments/field_update_dropdown.html'
	};
});

// Date field popover
AngularApp.directive('dateFieldPopover', function($timeout, $filter) {
	var uniqueId = 1;
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			html: '=dateFieldPopover',
			fieldValue: '=',
			modelId: '=',
			placeholder: '=',
			fieldString: '=',
			showRelativeDate: '=',
			handleSave: '&onSave'
		},
		requires: 'field',
		link: function(scope, elm, attr, controller) {
			scope.uniqueId = uniqueId++;
			scope.dateString = null;
			var inputId = 'date-field-popover-input-'+scope.uniqueId;
			elm.find('input').attr('id', inputId);

			if (scope.fieldValue) {
				scope.dateString = $filter('date')(scope.fieldValue, 'dd-MM-yyyy');
				if (scope.showRelativeDate)
					scope.dateString = moment(scope.fieldValue).calendar();
			}

			$timeout(function(){
				$("#"+inputId).datepicker({ autoclose: true, cssOpenClose: true})
					.datepicker('place')
					.on('changeDate', function (e) {
						scope.handleSave({'modelID': scope.modelId, 'field': scope.fieldString, 'value': e.date.toISOString()});
					});
			});

		},
		templateUrl: '/static/client/base/views/fragments/date_field_popover.html'
	};
});

// Date field popover
AngularApp.directive('dateFieldPopoverNgModel', function($timeout) {
	var uniqueId = 100;
	return {
		restrict: 'E',
		require: '?ngModel',
		scope: {
			ngModel: '=',
			nudgeRight: '=',
			positionRight: '='
		},
		requires: 'field',
		link: function(scope, elm, attr) {
			scope.uniqueId = uniqueId++;
			var inputId = 'date-field-popover-input-'+scope.uniqueId;
			elm.find('input').attr('id', inputId);

			var nowTemp = new Date();
			var _now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

			if (!scope.nudgeRight)
				scope.nudgeRight = 0;

			$timeout(function(){
				$("#"+inputId).datepicker({
					autoclose: true,
					cssOpenClose: true,
					extraClass: (scope.positionRight?'right':''),
					nudgeRight: scope.nudgeRight,
					onRender: function(date) {
						return date.valueOf() > _now.valueOf() ? 'disabled' : '';
					}})
					.datepicker('place')
					.on('changeDate', function (e) {
						scope.ngModel = e.date;
						scope.$apply();
					});
			});

		},
		templateUrl: '/static/client/base/views/fragments/date_field_popover_ngmodel.html'
	};
});


AngularApp.directive('tabset', function () {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    controller: function($scope) {
      $scope.templateUrl = '';
	  $scope.isActive = false;
      var tabs = $scope.tabs = [];
      var controller = this;

      this.selectTab = function (tab) {
        angular.forEach(tabs, function (tab) {
          tab.selected = false;
        });
        tab.selected = true;
      };

      this.setTabTemplate = function (templateUrl) {
        $scope.templateUrl = templateUrl;
		$scope.isActive = true;
      }

      this.addTab = function (tab) {
        if (tabs.length == 0) {
          controller.selectTab(tab);
        }
        tabs.push(tab);
      };
    },
    template:
      '<div class="row-fluid">' +
        '<div class="row-fluid">' +
          '<div class="nav nav-tabs" ng-transclude></div>' +
        '</div>' +
        '<div class="row-fluid">' +
			'<div class="item-body tab-pane tab-content">'+
          	'<ng-include src="templateUrl" class=""></ng-include>' +
			'</div>' +
        '</div>' +
      '</div>'
  };
});

AngularApp.directive('tab', function () {
  return {
    restrict: 'E',
    replace: true,
    require: '^tabset',
    scope: {
      title: '@',
      templateUrl: '@'
    },
    link: function(scope, element, attrs, tabsetController) {
      tabsetController.addTab(scope);

      scope.select = function () {
        tabsetController.selectTab(scope);
      }

      scope.$watch('selected', function () {
        if (scope.selected) {
          tabsetController.setTabTemplate(scope.templateUrl);
        }
      });
    },
    template:
      '<li ng-class="{active: selected}">' +
        '<a href="" ng-click="select()">{{ title }}</a>' +
      '</li>'
  };
});
