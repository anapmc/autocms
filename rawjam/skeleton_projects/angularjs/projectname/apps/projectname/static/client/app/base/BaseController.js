/**
 * Base controller for all controllers.
 * Use this as a template for all future controllers
 *
 * Use of Class.js
 *
 * @author tommy[followed by the usual sign]julo.ca
 */
var BaseController = Class.extend({

	$scope:null,
	$timeout:null,

	init:function(scope){
		this.$scope = scope;
		this.defineListeners();
		this.defineGlobalScope();
		this.defineScope();
	},


	/**
     * Initialize listeners needs to be overrided by the subclass.
     * Don't forget to call _super() to activate
     */
	defineListeners:function(){
		this.$scope.$on('$destroy',this.destroy.bind(this));
	},

	defineGlobalScope:function(){
		this.$scope.toJsDate = function(str){
			if(!str)return null;
			return new Date(str);
		}

        this.$scope.staticUrl = '/static/';
        this.$scope.mediaUrl = '/media/';

        this.$scope.safeApply = function(fn) {
            var phase = this.$root.$$phase;
            if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                    fn();
                }
            }
            else {
                this.$apply(fn);
            }
        };
	},
	defineScope:function(){
		// To be overwritten!
	},

	/**
     * Triggered when controller is about
     * to be destroyed, clear all remaining values.
     */
	destroy:function(event){
		//OVERRIDE
	}
})

BaseController.$inject = ['$scope', '$timeout', 'Restangular', 'djangoWebsocket'];
