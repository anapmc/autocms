var BaseModel = EventDispatcher.extend({
    // Private fields
    _updatingField: null,
    _updatingFieldValue: null,
    _updatingFieldIsM2M: null,
    _updatingFieldM2MType: null,
    _updatingFieldsDict: {},


    updateModelField: function(field, value, isM2M, m2mType){
        // field = name of the field being updated. value = value of the field. isM2M = set to true if this is an M2M field
        // m2mType = if an M2M field and being added, set to 1; if being deleted set to 0.
        this._updatingField = field;
        this._updatingFieldValue = value;
        this._updatingFieldIsM2M = isM2M;
        this._updatingFieldM2MType = m2mType;

        if (isM2M)
            field = 'm2m-'+(m2mType == 1 ? 'add' : 'delete')+'-dependencies';

        this._modelService.updateField(this.id, field, value)
            .then(this._handleUpdateModelFieldSuccess.bind(this),this._handleUpdateModelFieldError.bind(this));
    },
    updateModelFields: function(dict){
        this._updatingFieldsDict = dict;
        this._modelService.updateFields(this.id, dict)
            .then(this._handleUpdateModelFieldsSuccess.bind(this),this._handleUpdateModelFieldError.bind(this));
    },
    _handleUpdateModelFieldSuccess: function(result){
        var updatingFieldCamelCase = this._updatingField.replace('_','-').toCamel();
        if (this._updatingFieldIsM2M) {
            for (var i = 0; i < this._updatingFieldValue.length; i++) {
                if (this._updatingFieldM2MType == 1) {
                    this[this._updatingField].push(this._updatingFieldValue[i]);
                }
                else {
                    var index = this[updatingFieldCamelCase].indexOf(this._updatingFieldValue[i]);
                    if (index > -1) {
                        this[updatingFieldCamelCase].splice(index, 1);
                    }
                }
            }
        }
        else {
            if (this._updatingField == 'owner') {
                this.initOwner(result.owner);
            }
            else {
                this[updatingFieldCamelCase] = result[this._updatingField];
            }
        }
        somethingSaved();
    },
    _handleUpdateModelFieldsSuccess: function(result){
        for (var field in this._updatingFieldsDict) {
            var updatingFieldCamelCase = field.replace('_','-').toCamel();
            if (field == 'owner')
                this.initOwner(result.owner);
            else {
                this[updatingFieldCamelCase] = result[field];
            }
        }
        somethingSaved();
    },
    _handleUpdateModelFieldError: function(error){
        console.error('BaseModel : updateModelField error');
    },
})
