var ManageUsersController = BaseController.extend({
    _notifications:null,
    _userModel:null,

    init:function($scope, $timeout, UserModel, Notifications, $route){
        this.$timeout = $timeout;
        this._notifications = Notifications;
        this._userModel = UserModel;
        this._super($scope);
    },

    /*
    * Listeners
    */
    defineListeners:function(){
        this._super();
        this._notifications.addEventListener(users.events.USER_ADDED,this._handleUserAdded.bind(this));
    },
    _handleUserAdded:function(event, user){
    },

    /*
    * Scope properties and methods
    */
    defineScope:function(){
        var self = this;
        this.$scope.users = [];
        this.$scope.newUserEmail = null;
        this.$scope.newUserUsername = null;

        this.$scope.loadUsers = function() {
            self._userModel._userService.getAll().then(function(response) {
                response.forEach(function(user) {
                    var obj = {};
                    $.extend(obj, self._userModel);
                    obj.init(user[0]);
                    self.$scope.users.push(obj);
                });
            },
            function(response) {
                console.error('loadUsers: error');
            });
        }
        this.$scope.addUser = function() {
            self._userModel._userService.add({email:self.$scope.newUserEmail, username:self.$scope.newUserUsername}).then(function(response) {
                var obj = {};
                $.extend(obj, self._userModel);
                obj.init(response[0]);
                self.$scope.users.push(obj);
            },
            function(response) {
                console.error('addUser: error');
            });
        }
        this.$scope.deleteUser = function(user) {
            self._userModel._userService.delete(user).then(function(response) {
                self.$scope.users = _.reject(self.$scope.users, function(item) {
                    return item.id == user.id;
                });
            },
            function(response) {
                console.error('deleteUser: error');
            });
        }
    },

    /*
    * Clean up
    */
    destroy:function(){
        this._notifications.removeEventListener(users.events.USER_ADDED,this._handleUserAdded.bind(this));
    }
})

ManageUsersController.$inject = ['$scope', '$timeout', 'UserModel', 'Notifications', '$route'];
