var AuthService = BaseService.extend({

    getLoggedInUser:function(){
        var relations = {
            'profile': {}
        };
        var fields = ['first_name', 'last_name', 'email', 'username', 'id'];
        return this.restangular.oneUrl('loggedIn', '/ng/logged-in-user/').customGET('', {'relations':relations, 'fields':fields});
    }
});
AuthService.$inject = ['Restangular'];

(function (){

    var AuthServiceProvider = Class.extend({

        instance: new AuthService(),

        $get:['Restangular', function(Restangular){
            this.instance.restangular = Restangular.withConfig(function(Configurer) {
                //Configurer.setBaseUrl('/crud');
            });
            return this.instance;
        }],
    })

    angular.module('AuthService', [])
        .provider('AuthService', AuthServiceProvider);
}());
