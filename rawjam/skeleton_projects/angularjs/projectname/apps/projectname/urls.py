from django.conf.urls import url, patterns

from views import Home
from apps.projectname_profile.views import AngularUserCRUDView

urlpatterns = patterns('',
	url(r"^$", Home.as_view(),  name="home"),
)

# AngularJS CRUD URL's
urlpatterns += patterns('',
	url(r'^crud/users/$', AngularUserCRUDView.as_view(), name='angular_user_crud_view'),
	url(r'^crud/users/(?P<pk>\d+)/$', AngularUserCRUDView.as_view(), name='angular_user_crud_view')
)
