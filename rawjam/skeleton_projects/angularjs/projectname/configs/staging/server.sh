#!/bin/bash
set -e
LOGFILE=/home/ubuntu/logs/projectname_gunicorn.access.log
ERRORFILE=/home/ubuntu/logs/projectname_gunicorn.error.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=3  #recommended formula here is 1 + 2 * NUM_CORES

ADDRESS=unix:/var/run/projectname.sock

cd /home/ubuntu/sites/projectname/repository
source ../env/bin/activate
export PYTHONPATH=$PYTHONPATH:/home/ubuntu/sites/projectname/repository/django-angular
export PYTHONPATH=$PYTHONPATH:/home/ubuntu/sites/projectname/repository/rawjam
export PYTHONPATH=$PYTHONPATH:/home/ubuntu/sites/projectname/repository/projectname
test -d $LOGDIR || mkdir -p $LOGDIR
exec ../env/bin/gunicorn_django -w $NUM_WORKERS --bind=$ADDRESS \
	--log-level=debug \
	--log-file=$LOGFILE 2>>$LOGFILE  1>>$ERRORFILE \
	--settings=projectname.configs.staging.settings