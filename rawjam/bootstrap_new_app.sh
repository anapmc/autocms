#!/bin/bash

echo "Hello, "$USER".  This script will create a new Skeleton App in the active project."
echo "Enter the name of this project (as it appears in Spring Loops):"
read projectname
echo "Enter the name of the app that you'd like to create:"
read appname
echo

apps_root_path="${PWD}/${projectname}/apps";
app_path="${PWD}/${projectname}/apps/${appname}";
root="${PWD}";

if [ ! -d "${app_path}" ]; then
	cd ${apps_root_path};
	cp -a ${root}/rawjam/example_project/projectname/apps/sample_app/. sample_app;
	
	
	mv sample_app/templates/modelname/modelname_confirm_delete.html sample_app/templates/modelname/${appname}_confirm_delete.html;
	mv sample_app/templates/modelname/modelname_detail.html sample_app/templates/modelname/${appname}_detail.html;
	mv sample_app/templates/modelname/modelname_form.html sample_app/templates/modelname/${appname}_form.html;
	mv sample_app/templates/modelname/modelnames_index.html sample_app/templates/modelname/${appname}s_index.html;
	mv sample_app/templates/modelname sample_app/templates/${appname};
	mv sample_app ${appname};
	
	#sed_cmd="s/projectname/${projectname}/g";
	#find . -type d \( -name rawjam -o -name allauth -o -name .git \) -prune -o -name "*.py" -print -exec sed -i '' -e "$sed_cmd" {} +
fi

echo "App '${appname}' successfully created."