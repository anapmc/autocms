#!/bin/bash

echo "Hello, "$USER".  This script will create the base Raw Jam Django project for you. This should only be done for NEW / EMPTY git projects."
echo "Enter the project name exactly as it appears in Spring Loops: "
read projectname
echo "What type of project is this (basic, cms, angularjs)? "
read projecttype
echo "What is the username of your local postgreSQL user? "
read postgresqluser
echo

root=${PWD}
proj_path="${root}/${projectname}";
rawjam_path="${root}/rawjam";

if [ ! -d "${proj_path}" ]; then
	# Switch RawJam to the stable branch
	cd "${rawjam_path}";
	git checkout stable;
	git pull origin stable;
	cd "${root}";

	mkdir "${projectname}";
	cd "${proj_path}";
	git init;
	git remote add origin "git@bitbucket.org:rawjampeople/${projectname}.git";
	git config remote.origin.push refs/heads/master:refs/heads/master;
	cp ${rawjam_path}/core/gitignore_bootstrap .gitignore;

	if [ ! -d "${proj_path}/${projectname}" ]; then
		cp -a ${rawjam_path}/skeleton_projects/${projecttype}/. .;
		mv projectname/apps/projectname_profile projectname/apps/${projectname}_profile;
		mv projectname/apps/projectname/templates/projectname projectname/apps/PROJECTNAME/templates/${projectname};
		mv projectname/apps/projectname projectname/apps/${projectname};
		mv projectname ${projectname};

		sed_cmd="s/projectname/${projectname}/g";
		find . -type d \( -name rawjam -o -name allauth -o -name .git \) -prune -o -name "*.py" -print -exec sed -i '' -e "$sed_cmd" {} +
		find . -type d \( -name rawjam -o -name allauth -o -name .git \) -prune -o -name "*.sh" -print -exec sed -i '' -e "$sed_cmd" {} +
		find . -type d \( -name rawjam -o -name allauth -o -name .git \) -prune -o -name "*.html" -print -exec sed -i '' -e "$sed_cmd" {} +
        find . -name "manage" -print -exec sed -i '' -e "$sed_cmd" {} +

		# Add the rawjam submodule and make sure we're on the stable branch
		git submodule add git@bitbucket.org:rawjampeople/rawjam.git rawjam;
		cd "${proj_path}/rawjam";
		git checkout stable;
		cd "${proj_path}";

		# Add the django-angular submodule?
		if [ "${projecttype}" = "angularjs" ]; then
			git submodule add https://github.com/rawjam/django-angular.git;
		fi

		git add .;
		git commit -am 'Initial commit';
		git push origin master;

		virtualenv "${proj_path}/env";
		source "${proj_path}/env/bin/activate";
		pip install -r "${proj_path}/requirements.txt";

		# Create the database
		createdb -h localhost -U ${postgresqluser} "${projectname}";

		sh "${proj_path}/manage" migrate auth;
		sh "${proj_path}/manage" syncdb --noinput;
		sh "${proj_path}/manage" migrate;
		sh "${proj_path}/manage" loaddata "${rawjam_path}/allauth.socialaccount.initial.json";
		sh "${proj_path}/manage" createsuperuser;
	fi
fi

echo "Project '${projectname}' successfully created. Others can now clone it locally. Goodbye :)"
