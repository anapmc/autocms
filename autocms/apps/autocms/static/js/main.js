/**
 * Created by anapmc on 14/03/16.
 */
document.addEventListener('DOMContentLoaded', function() {
    function getCookie(name) {
        var cookieValue = null;
        var i = 0;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (i; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

});
//var filter_settings={
//    make:0,
//
//}

var make_filter = "";
var type_filter = "";
var refreshCars = function(){
    $.ajax({
        type: "GET",
        url: "/veiculos/filter",
        data: { "itemsperpage": $('#page').val(), "fuel": $('#fuel-type-div div').html(),
            "sort":$('#sort').attr('data-text'),'make': make_filter, 'type': type_filter,
            "min_price":$("#slider-price_min").val(), "max_price":$("#slider-price_max").val()},
        success: function(data) {
            $("#list-all-cars").html(data);
        }
    });

};

$('#page').on('change', function (e) {
    refreshCars();

});

$('#fuel-type').on('change', function (e) {
     e.preventDefault();
    refreshCars();
});


$('#sort').on('change', function (e) {
    e.preventDefault();
    refreshCars();
});

$(".make-link").on('click',function(e){
    e.preventDefault();
    make_filter = $(this).find("span").html();
    $.each($(".make-link"), function(){
        $(this).removeClass("selected");
    });
    $(this).addClass("selected");
    refreshCars();
});

$(".type-link").on('click',function(e){
    e.preventDefault();
    type_filter = $(this).find("span").html();
    $.each($(".type-link"), function(){
        $(this).removeClass("selected");
    });
    $(this).addClass("selected");
    refreshCars();
});

$("#price-filter").on('click', function (e) {
    e.preventDefault();
    refreshCars();

});