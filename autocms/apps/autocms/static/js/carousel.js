$(document).ready(function() {
    var slideStatus = true; //true if the slide is running, false if not

    $("#slide-pause-btn").click(function(){
        if (slideStatus){
            $("#homepage-carousel").carousel('pause');
            slideStatus = false;
            $("#slide-pause-btn").attr("class", "fa fa-play");

        }
        else {
            $("#homepage-carousel").carousel('next');
            $("#homepage-carousel").carousel('cycle');
            $("#slide-pause-btn").attr("class", "fa fa-pause");
            slideStatus = true;

        }
    });

   if ($("#homepage-carousel").length) {
       //add class active to first item of carousel!!!
       $($("#carousel-inner").find(".item")[0]).attr("class", "item active");
       $("#homepage-carousel").carousel('cycle');
   }

});