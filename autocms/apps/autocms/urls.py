from apps.cars.views import VehicleListView, FilterVehicleView, Home, ScrapeStandVirtualView, \
    VehicleDetailView
from django.conf.urls import url, include
from django.contrib import admin




urlpatterns = [
	url(r"^$", Home.as_view(),  name="home"),
	url(r'^admin/', include(admin.site.urls)), # admin site
	url(r'^scrape/standvirtual/', ScrapeStandVirtualView.as_view()), # admin site
    url(r'veiculos$', VehicleListView.as_view()),
    url(r'veiculos/filter$', FilterVehicleView.as_view()),
    url(r'veiculos/(?P<slug>[\w-]+)$', VehicleDetailView.as_view()),

]
