from django.template.defaulttags import register



@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

# from django import template
#
# register = template.Library()

@register.filter
#capitalise the first letter of each sentence in a string
def capsentence(value):
    value = value.lower()
    return ". ".join([sentence.capitalize() for sentence in value.split(". ")])