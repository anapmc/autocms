from django.core.management.base import BaseCommand, CommandError
from apps.cars.scrappers import scrapper_standvirtual

class Command(BaseCommand):

    def handle(self, *args, **options):
        print "Start scrapping..."
        scrapper_standvirtual()
        print "Scraping finished."
