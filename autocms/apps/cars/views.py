from itertools import chain

from apps.cars.scrappers import scrapper_standvirtual
from django.http import HttpResponse
from django.http.response import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from .models import CarouselIMG,Car, Make, VehicleType, FeaturesGroup
# from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.

from django.views.generic import View


class ScrapeStandVirtualView(View):
    def get(self, request, *args, **kwargs):
        try:
            status = "ok"
            scrapper_standvirtual()
        except Exception, e:
            status = "error: %s" % e
        return JsonResponse({"status": status})

class Home(TemplateView):
    template_name = "autocms/home.html"


    def get_context_data(self, **kwargs):
        most_recent = Car.objects.filter(featured_grid=True)
        count = 6-most_recent.count()
        if (count)>0:
            most_recent = list(chain(most_recent, Car.objects.filter(published=True).order_by('-published_date')[:count]))
        context = {
            # 'images': Car.objects.filter(featured=True),
            'cars': Car.objects.filter(published=True, featured=True),
            'most_recent': most_recent
        }
        return context

class VehicleListView(ListView):
    model = Car
    queryset = Car.objects.filter(published=True).order_by("-published_date")
    # paginate_by = 5
    template_name = "autocms/vehicle-listings.html"


    def get_context_data(self, **kwargs):
        context = super(VehicleListView, self).get_context_data(**kwargs)
        context['makers'] = Make.objects.all()
        context['types'] = VehicleType.objects.all()
        return context



class FilterVehicleView(View):
    template_name = 'autocms/fragments/car-listing-item.html'
    paginate_by = 5

    def get(self, request, *args, **kwargs):
        cars_list = Car.objects.filter(published=True)
        args = request.GET
        if args.get('itemsperpage'):
            itemsperpage = args.get('itemsperpage')
        if args.get('fuel')!="Todos":
            if args.get('fuel') == "Gasolina":
                fuel = Car.GASOLINE
            else:
                fuel = Car.DIESEL
            cars_list = cars_list.filter(fuel=fuel)
        if args.get('make') and args.get('make') != "Todos":
            make = Make.objects.get(title=args.get('make'))
            cars_list = cars_list.filter(make=make)
        if args.get('type') and args.get('type') != "Todos":
            type = VehicleType.objects.get(title=args.get('type'))
            cars_list = cars_list.filter(type=type)
        min_price = int(args.get('min_price')[1:])
        max_price = int(args.get('max_price')[1:])
        cars_list = cars_list.filter(price__gte=min_price, price__lte=max_price)
        if args.get('sort'):
            if args.get('sort') == u"Mais baratos":
                cars_list = cars_list.order_by("price")
            elif args.get('sort') == u"Mais caros":
                cars_list = cars_list.order_by("-price")
            else:
                cars_list = cars_list.order_by("-published_date")


        # paginator = Paginator(cars_list, 5) # Show 25 contacts per page
        # page = args.get('page')
        # try:
        #     cars = paginator.page(page)
        # except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        # cars = paginator.page(1)
        # except EmptyPage:
        #     If page is out of range (e.g. 9999), deliver last page of results.
        # contacts = paginator.page(paginator.num_pages)
        context = {'car_list': cars_list}
        html = render_to_string(self.template_name, context)
        return HttpResponse(html)


class VehicleDetailView(DetailView):
    model = Car
    template_name = "autocms/car-details.html"


    def get_context_data(self, **kwargs):
        context = super(VehicleDetailView,self).get_context_data(**kwargs)
        all_features = {}
        for g in FeaturesGroup.objects.all():
            all = g.get_features_of_car(context['car'])
            if all:
                all_features[g.title] = all
        context.update({'all_features':all_features})
        return context
