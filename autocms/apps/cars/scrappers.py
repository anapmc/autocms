# -*- coding: utf-8 -*-
# encoding=utf8
import re
from apps.cars.models import CarImage, Car, Make, VehicleType, ExtraFeatures, FeaturesGroup
import bs4, requests
import string as strlib
# import urllib3
import certifi

import ssl

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.poolmanager import PoolManager

#
# class Ssl3HttpAdapter(HTTPAdapter):
#     """"Transport adapter" that allows us to use SSLv3."""
#
#     def init_poolmanager(self, connections, maxsize, block=False):
#         self.poolmanager = PoolManager(
#                 num_pools=connections,
#                 maxsize=maxsize,
#                 block=block,
#                 ssl_version=ssl.PROTOCOL_SSLv3,
#                 cert_reqs='CERT_REQUIRED', # Force certificate check.
#                 ca_certs=certifi.where(),  # Path to the Certifi bundle.
#
#         )


# You're ready to make verified HTTPS requests.
# try:
#     r = http.request('GET', 'https://example.com/')

import sys
reload(sys)
sys.setdefaultencoding('utf8')

chrome_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36'

HEADERS ={
    'user-agent': chrome_agent
}

PROXIES = {
    'http': 'http://185.7.14.26:8118',
    'https': 'http://185.28.193.95:8080',
}

def remove_spaces(string):
    for sp in ['\r','\n', '\t']:
        string.replace(sp,"")
    return string
def lettersOnly(str_to_parse, lower=False):
    """

    Args:
        str_to_parse: string with combination of chars

    Returns: Returns the same string removing all chars except letters

    """
    all = strlib.maketrans("","")
    noletters = all.translate(all, strlib.letters)
    result = str_to_parse.encode('UTF-8').translate(all, noletters)
    if lower:
        return result.lower()
    return result

def letters_and_digits(str_to_parse):
    all = strlib.maketrans("","")
    noletters = all.translate(all, strlib.letters+strlib.digits)
    result = str_to_parse.encode('UTF-8').translate(all, noletters)
    return result.lower()



def scrappe_car(car_url):
    response = requests.get(car_url, headers=HEADERS)
    soup = bs4.BeautifulSoup(response.text, "lxml")
    name = soup.find('h3').text
    year = soup.find(string=re.compile("Primeiro Registo",re.IGNORECASE)).parent.next_sibling.next_element
    price = soup.find('h2').text.split()[1].replace(".","").replace(",",".")
    km = soup.find(string=re.compile("Quil", re.IGNORECASE)).parent.next_sibling.next_element
    km = int(remove_spaces(km.replace(".","")))
    year = remove_spaces(year)
    power = soup.find(string=re.compile("Pot", re.IGNORECASE)).find_next('span')['title']
    try:
        type, _ = soup.find("div", {"class": "omega"}).find("div").string.split(",")
    except:
        type = soup.find("div", {"class": "omega"}).find("div").string.split(",")[0]
    print type
    description = ''
    try:
        description, _ = soup.find(string=re.compile(u'Crédito', re.IGNORECASE)).split(u"Crédito")
    except Exception, e:
        print "description failed %s" % e
    fuel = Car.GASOLINE
    if soup.find('img',{'title':"Diesel"}):
        fuel = Car.DIESEL
    color = soup.find(string=re.compile("Cor:", re.IGNORECASE)).parent.next_sibling.next_element
    color = remove_spaces(str(color))
    imgs_gallery = soup.find(id='rg-gallery').find('ul')
    imgs = []
    for img in imgs_gallery.find_all('li'):
        imgs.append(img.find('img')['data-large'])
    #GET FEATURES
    features = {}
    h3features = soup.find(string=re.compile("Equipamento", re.IGNORECASE))
    info_geral_div = h3features.parent.next_sibling.find_all("div")
    features['info_geral'] = []
    for feature_div in info_geral_div:
        features['info_geral'].append(feature_div.text)
    extra_features_divs = soup.find_all("div", {"class":"extra"})
    for section in extra_features_divs:
        label = section.text.split(":")[0]
        if label.encode("UTF-8").startswith("Extra"):
            label = "opcoes_extra"
        elif label.encode("UTF-8").startswith("Luzes"):
            label = "luzes"
        elif label.encode("UTF-8").startswith("Bancos"):
            label = "bancos"
        elif label.encode("UTF-8").startswith("Conforto"):
            label = "conforto_design"
        elif label.encode("UTF-8").startswith("Volante"):
            label = "volantes"
        elif label.encode("UTF-8").startswith("Retrovisores"):
            label = "retrovisores"
        elif label.encode("UTF-8").startswith("Multim"):
            label = "multimedia"
        elif label.encode("UTF-8").startswith("Exterior"):
            label = "exterior"
        elif label.encode("UTF-8").startswith("Segurança Activa"):
            label = "seguranca_ativa"
        elif label.encode("UTF-8").startswith("Segurança Passiva"):
            label = "seguranca_passiva"
        label = label.replace(" ", "_")
        features[label] = []
        feature_divs = section.find_all("div")
        for feature_div in feature_divs:
            features[label].append(feature_div.text)

    return {'name': name, \
            'year': year, \
            'power': int(power[:-2]), \
            'description': description, \
            'price': int(float(price)), \
            'color': color, \
            'imgs': imgs, \
            'km': km, \
            'type': type, \
            'features':features, \
            'fuel':fuel}


def scrapper_standvirtual():
    report_msg = "Relatorio de atualizacoes standvirtual\n\n"
    root_url = 'http://home.standvirtual.com/bertocarautomoveis_listagem.html'
    print "Waiting response...."
    # s = Session()
    response = requests.get(root_url, headers=HEADERS)
    print "Response"
    soup = bs4.BeautifulSoup(response.text, "lxml")
    next_page = True
    while( next_page ):
        for car in soup.find_all('div', {"class":"ibox"}):
            car_link = car.find('a')['href']
            car_make = car.find('a').text.encode("UTF-8").split("\xc2")[0]
            try:
                make, _ = Make.objects.get_or_create(
                        cmp=lettersOnly(car_make, True),
                        defaults={
                            'title': car_make.replace(" ", "")
                        }
                )
                print "Scraping car"
                car_details = scrappe_car(car_link)
                print "Car details %s" % car_details
                type, _ =VehicleType.objects.get_or_create(
                        cmp=lettersOnly(car_details['type'], True),
                        defaults = {
                            'title': car_details['type']
                        }
                )
                car_obj, created = Car.objects.update_or_create(
                        make=make,
                        model=car_details['name'],
                        first_record=car_details['year'],
                        defaults={
                            'horse_power': car_details['power'],
                            'description': car_details['description'],
                            'price': car_details['price'],
                            'color': car_details['color'],
                            'mileage': car_details['km'],
                            'published': True,
                            'type' : type,
                            'fuel': car_details['fuel']
                        }
                )
                if created:
                    report_msg += "- O carro %s foi adicionado.\n" % car_details['name']
                else:
                    report_msg += "- O carro %s foi atualizado.\n" % car_details['name']
                print car_details["name"]
                first = True
                car_obj.save()
                orde=0
                for group in car_details['features']:
                    group_obj, _ = FeaturesGroup.objects.get_or_create(
                            cmp=letters_and_digits(group),
                            defaults={
                                'title': group,
                                'order': orde,
                            }

                    )
                    orde+=1
                    group_obj.save()
                    for feature_elem in car_details['features'][group]:
                        feature, _ = ExtraFeatures.objects.get_or_create(
                                cmp=letters_and_digits(feature_elem),
                                defaults={
                                    'title': feature_elem,
                                    'group': group_obj
                                }
                        )
                        feature.save()
                        car_obj.extra_features.add(feature)
                order = 0
                for img in car_details['imgs']:
                    main = False
                    if first:
                        main = True
                        first = False
                    carimg_obj, created = CarImage.objects.get_or_create(
                            image_url=img,
                            defaults={
                                'car': car_obj,
                                'main': main,
                                'order': order,
                            }
                    )
                    carimg_obj.save()
                    order += 1
            except Exception, e:
                report_msg += "Aconteceu algo de errado: %s \n" % e
        next_page_url = soup.find(id="pag_next")
        if next_page_url:
            root_url = next_page_url['href']
            response = requests.get(root_url, headers=HEADERS)
            soup = bs4.BeautifulSoup(response.text, "lxml")
        else:
            next_page = False

    requests.post(
            "https://api.mailgun.net/v3/sandbox3258362d07ad4c98a23f2608e8c35dee.mailgun.org/messages",
            auth=("api", "key-329b569e10ce966b2ab24b3ede2c25c1"),
            data={"from": "Bertocar Atualizacoes <postmaster@sandbox3258362d07ad4c98a23f2608e8c35dee.mailgun.org>",
                  "to": "Ana <anapmc.carvalho@gmail.com>",
                  "subject": "Carros atualizados",
                  "text": report_msg
                  })
    requests.post(
            "https://api.mailgun.net/v3/sandbox3258362d07ad4c98a23f2608e8c35dee.mailgun.org/messages",
            auth=("api", "key-329b569e10ce966b2ab24b3ede2c25c1"),
            data={"from": "Bertocar Atualizacoes<postmaster@sandbox3258362d07ad4c98a23f2608e8c35dee.mailgun.org>",
                  "to": "Bertocar <geral@bertocar.pt>",
                  "subject": "Carros atualizados",
                  "text": report_msg
                  })


    # except:
    #      print "Something went wrong with car ", car_details['name']


