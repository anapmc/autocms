from django.contrib import admin

# Register your models here.


from .models import Car, CarImage, CarouselIMG, Make, VehicleType, ExtraFeatures, FeaturesGroup


class CarImageInline(admin.TabularInline):
    model = CarImage
    extra = 3
    fields = ('image','image_tag', 'main')
    readonly_fields = ('image_tag',)


class CarAdmin(admin.ModelAdmin):
    exclude = ('slug',)
    list_display = ('model', 'published', 'credit', 'price', 'featured', 'featured_grid')
    list_editable = ('credit', 'published', 'price', 'featured', 'featured_grid')
    inlines = [ CarImageInline, ]

class MakeAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_editable = ('title',)

class CarouselIMGAdmin(admin.ModelAdmin):
    list_display = ('image_tag','description', 'published')
    fields = ('image', 'image_tag','description', 'published')
    list_editable = ('description', 'published')
    readonly_fields = ('image_tag',)

class VehicleTypeAdmin(admin.ModelAdmin):
    list_display = ('title',)

class ExtraFeaturesAdmin(admin.ModelAdmin):
    list_display = ('title','icon', )
    exclude = ('cmp', 'iden')
    list_editable = list_display

class FeaturesGroupAdmin(admin.ModelAdmin):
    list_display = ('title','icon', )
    exclude = ('cmp', )
    list_editable = ('title','icon', )


admin.site.register(Car, CarAdmin)
admin.site.register(Make, MakeAdmin)
admin.site.register(CarouselIMG, CarouselIMGAdmin)
admin.site.register(VehicleType, VehicleTypeAdmin)
admin.site.register(ExtraFeatures, ExtraFeaturesAdmin)
admin.site.register(FeaturesGroup, FeaturesGroupAdmin)
