# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-04-13 07:26
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0023_auto_20160413_0725'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='car',
            options={'ordering': ['-published', '-published_date'], 'verbose_name': 'Carro', 'verbose_name_plural': 'Carros'},
        ),
    ]
