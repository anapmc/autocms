# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-04-05 08:09
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0005_auto_20160404_2326'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='abs',
            field=models.BooleanField(default=False, verbose_name=b'Airbags'),
        ),
        migrations.AddField(
            model_name='car',
            name='extra_features',
            field=models.ManyToManyField(to='cars.ExtraFeatures', verbose_name=b'Equipamento'),
        ),
        migrations.AddField(
            model_name='extrafeatures',
            name='cmp',
            field=models.CharField(default=b'', max_length=200),
        ),
        migrations.AlterField(
            model_name='car',
            name='published_date',
            field=models.DateField(default=datetime.date(2016, 4, 5), verbose_name=b'Data de publicacao'),
        ),
    ]
