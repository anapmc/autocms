from django.db import models
from django.utils.functional import lazy
from django.utils.text import slugify
import datetime
from django.core.files import File
import os, urllib
import string as strlib


class VehicleType(models.Model):
    title = models.CharField(default="", max_length=200)
    cmp = models.CharField(default="", max_length=200)

    class Meta:
        verbose_name = u'Tipo'
        verbose_name_plural = u'Tipos'

    def get_total(self):
        return len(self.car.all())

    def __unicode__(self):
        return self.title

    # def __eq__(self, other):
    #     return self.cmp == other.cmp


class Make(models.Model):
    title = models.CharField(default="", max_length=200)
    cmp = models.CharField(default="", max_length=200)

    class Meta:
        verbose_name = u'Marca'
        verbose_name_plural = u'Marcas'

    def get_total(self):
        return len(self.car.all())


    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.cmp == "vw":
            self.title = "Volkswagen"
        if self.cmp == "alfa":
            self.title = "Alfa Romeo"
        super(Make, self).save(*args, **kwargs)
    #
    # def __eq__(self, other):
    #     return self.cmp == other.cmp


class FeaturesGroup(models.Model):
    title = models.CharField(default="", max_length=200)
    cmp = models.CharField(default="", max_length=200)
    icon = models.CharField(default="", max_length=200, blank=True, null=True)
    order = models.IntegerField(verbose_name="Ordem",default=0)



    def get_features_of_car(self, car_obj):
        return self.features.filter(car=car_obj)

    class Meta:
        ordering = ['order']

    def __unicode__(self):
        return self.title


#check ss amenities
class ExtraFeatures(models.Model):
    title = models.CharField(default="", max_length=200)
    ident = models.IntegerField(default=0)
    cmp = models.CharField(default="", max_length=200)
    icon = models.CharField(default="", max_length=200)
    order = models.IntegerField(verbose_name="Ordem",default=0)
    group = models.ForeignKey(FeaturesGroup, related_name="features", null=True)

    class Meta:
        ordering = ['order']

    def __unicode__(self):
        return self.title

    # def __eq__(self, other):
    #     return self.cmp == other.cmp




class Car(models.Model):
    DIESEL = 0
    GASOLINE = 1
    FUEL = (
        (DIESEL, "Diesel"),
        (GASOLINE, "Gasolina")
    )
    make = models.ForeignKey(Make, related_name="car")
    model = models.CharField(default="", max_length=200, verbose_name="Modelo")
    description = models.CharField(max_length=255, default="no description", verbose_name="Descricao")
    published = models.BooleanField(default=True, verbose_name="Visivel")
    price = models.IntegerField(default=0, verbose_name="Preco")
    published_date = models.DateField(verbose_name="Data de publicacao")
    mileage = models.IntegerField(default=0, verbose_name="KM")

    color = models.CharField(max_length=255, verbose_name="Cor")
    first_record = models.CharField(default="", max_length=255, verbose_name="Primeiro Registo")
    slug = models.SlugField()
    credit = models.IntegerField(null=True, blank=True, verbose_name="Credito a partir de")
    featured = models.BooleanField(default=False, verbose_name="Slider")
    featured_grid = models.BooleanField(default=False, verbose_name="Mais recentes")
    fuel = models.IntegerField(choices=FUEL, default=GASOLINE, verbose_name="Combustivel")
    horse_power = models.IntegerField(default=0, verbose_name="Potencia")
    type = models.ForeignKey(VehicleType, null=True, blank=True, related_name="car")

    #extra
    extra_features = models.ManyToManyField(ExtraFeatures, related_name="car", verbose_name="Equipamento")
    # show_credit_info = models.BooleanField(default=False)

    class Meta:
        verbose_name = u'Carro'
        verbose_name_plural = u'Carros'
        ordering = ['-published', '-published_date']

    def get_main_img_url(self):
        all_imgs = self.images
        main = all_imgs.filter(main=True)
        try:
            return main[0].get_url()
        except:
            if all_imgs.all():
                return all_imgs.all()[0]
            return None

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.model)
        if not self.published_date:
            self.published_date = datetime.datetime.today()
        if not self.make.title in self.model:
            self.model = "%s %s" % (self.make, self.model)
        super(Car, self).save(*args, **kwargs)


    def get_all_imgs(self):
        return [img.get_url() for img in CarImage.objects.filter(car=self.id)]

    # def main_image_tag(self):
    #      return u'<img src="%s" />' % self.get_url()
    # main_image_tag.short_description = 'Imagem principal'
    # main_image_tag.allow_tags = True
    def __unicode__(self):
        return self.model


class CarImage(models.Model):
    car = models.ForeignKey(Car, related_name="images")
    image = models.ImageField(null=True, blank=True, upload_to='carros')
    image_url = models.URLField(null=True, blank=True,)
    main = models.BooleanField(default=False, verbose_name="Foto principal")
    order = models.IntegerField(verbose_name="Ordem", default=0)

    class Meta:
        ordering = ['order']


    def get_path(self):
        if self.image.path:
            return self.image.path
        return None

    def get_url(self):
        if self.image.url:
            return self.image.url
        return None

    def image_tag(self):
        return u'<img src="%s" />' % self.get_url()

    image_tag.short_description = 'Fotos do carro'
    image_tag.allow_tags = True

    def get_remote_image(self):
        if self.image_url and not self.image:
            result = urllib.urlretrieve(self.image_url)
            self.image.save(
                    os.path.basename(self.image_url),
                    File(open(result[0]))
                    )
            self.save()

    def save(self, *args, **kwargs):
        super(CarImage, self).save(*args, **kwargs)
        self.get_remote_image()



class CarouselIMG(models.Model):
    image = models.ImageField(null=True, blank=True, upload_to='carousel')
    description = models.CharField(max_length=200)
    published = models.BooleanField(default=True)
    class Meta:
        verbose_name = u'Imagem do Slide'
        verbose_name_plural = u'Imagens do Slide'

    def get_path(self):
        if self.image.path:
            return self.image.path
        return None

    def get_url(self):
        if self.image.url:
            return self.image.url
        return None

    def image_tag(self):
        return u'<img src="%s" class="img-responsive"/>' % self.get_url()

    image_tag.short_description = 'Imagem do Slide'
    image_tag.allow_tags = True
