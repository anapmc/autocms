#!/bin/bash
set -e
LOGFILE=/home/ubuntu/logs/autocms_gunicorn.access.log
ERRORFILE=/home/ubuntu/logs/autocms_gunicorn.error.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=3  #recommended formula here is 1 + 2 * NUM_CORES

ADDRESS=127.0.0.1:8000

cd /home/ubuntu/sites/autocms/repository
source ../env/bin/activate
export PYTHONPATH=$PYTHONPATH:/home/ubuntu/sites/autocms/repository/rawjam
export PYTHONPATH=$PYTHONPATH:/home/ubuntu/sites/autocms/repository/autocms
test -d $LOGDIR || mkdir -p $LOGDIR
exec gunicorn -w $NUM_WORKERS --bind=$ADDRESS \
	--log-level=debug \
	--log-file=$LOGFILE 2>>$LOGFILE  1>>$ERRORFILE \
	--settings=autocms.configs.staging.settings \
	--user=$USER \
	autocms.configs.staging.wsgi:application
