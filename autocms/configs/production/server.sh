#!/bin/bash
set -e
LOGFILE=/home/ubuntu/logs/autocms_gunicorn.access.log
ERRORFILE=/home/ubuntu/logs/autocms_gunicorn.error.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=3  #recommended formula here is 1 + 2 * NUM_CORES

#we don't want to run this as root..
USER=www-data
GROUP=www-data

ADDRESS=unix:/var/run/autocms.sock

cd /home/ubuntu/sites/autocms/repository
source ../env/bin/activate
export PYTHONPATH=$PYTHONPATH:/home/ubuntu/sites/autocms/repository/rawjam
export PYTHONPATH=$PYTHONPATH:/home/ubuntu/sites/autocms/repository/autocms
test -d $LOGDIR || mkdir -p $LOGDIR
exec gunicorn_django -w $NUM_WORKERS --bind=$ADDRESS \
	--log-level=debug \
	--log-file=$LOGFILE 2>>$LOGFILE  1>>$ERRORFILE \
	--settings=autocms.configs.production.settings \
	--user=$USER --group=$GROUP
